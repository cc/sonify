code repository for sonification workshops, holds a copy of all demo code running at
> https://ccrma.stanford.edu/~cc/sonify

uses the custom **timeWorkers** framework (JavaScript)

# documentation
paper on [Browser-based Sonification](https://lac.linuxaudio.org/2019/doc/chafe.pdf) for [Linux Audio Conference](https://lac.linuxaudio.org/2019)

# run from web
firefox, chrome, edge,
point browser to [demo directory](https://ccrma.stanford.edu/~cc/sonify) where there's an index.html file that plays an example data file from the [data/](https://ccrma.stanford.edu/~cc/sonify/data) subdirectory

# run from local copy
copy a read-only snapshot with the download button
open index.html with firefox
index.html plays a (local) data file using the sonify function in index.html

****
this is a public project on
> https://cm-gitlab.stanford.edu/public

clone to a repository on a local machine 
> git clone git@cm-gitlab.stanford.edu:cc/sonifyjs.git